package main

import "fmt"

func main(){
	input := []string{"apple","pie","apple","red","red","red"}

	/// I have created some test inputs , please don't forget to check them .

	//input := []string{"gorkem","erdem","erdem","erdem","gorkem"}
	//input := []string{"pc","paper","phone","cup","pencil","pc"}
	//input := []string{"mercedes","bmw","mercedes","bmw"}
	//input := []string{"C++","JS","C++","Golang","Java","JS"}

	MostRepeatedString(input)	                 /// Function takes an array as parameter 
}

func MostRepeatedString(input []string){         
	MostRepeated := ""                           /// This variable keep most repeated string
	MaxRepeat:= 0                                /// This variable keep number of repeat of most repeated string

	for i := 0; i < len(input); i++ {            /// With this loop , we can iterate each string
		Repeat := 1                              /// Repeat count set to 1 . Because if we found two same strings ,
		                                         /// we increase this variable and we have got two same strings . 

		for k := i+1; k < len(input); k++ {      /// I have used this loop for comparing strings with each other . 
			
			if input[i] == input[k] {            /// If two strings are matched , repeat increased .
				Repeat++		
			}
		}
		if Repeat>MaxRepeat {                    /// If repeat number bigger than max repeat number , MaxRepeat set to repeat
			MaxRepeat = Repeat                   /// and MostRepeated set to string that i. index in input array .
			MostRepeated = input[i]              
		}else if Repeat==MaxRepeat {             /// If most repeated strings are more than one ,
			MostRepeated += " "+input[i]         /// these strings add to MostRepeated variable and they are shown .
		}
		
	}

	fmt.Println(MostRepeated)                    /// Results . 
}
